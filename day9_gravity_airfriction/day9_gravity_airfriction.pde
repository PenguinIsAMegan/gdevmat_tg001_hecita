Mover mover;
int qty = 5;
Mover[] movers = new Mover[qty];

void setup()
{
  size(1080, 720, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  
  mover = new Mover();
  mover.position.x = Window.left + 50;
  float posY = Window.top - 50;
  //mover.mass = 10;
  //mover.acceleration = new PVector(0.1, 0);
  
  for (int i = 0; i < qty; i++)
  {
    movers[i] = new Mover(Window.left + 50, posY);
    movers[i].mass = random(1, 10);
    movers[i].setColor(random(1, 255),random(1, 255),random(1, 255), random(150, 255));
    posY -= 150;
  }
}

PVector wind = new PVector(0.1f, 0);

void draw()
{
  background(255);
  
  noStroke();
  for (Mover m : movers)
  {
    //m.applyGravity();
    //m.applyFriction();
    
    m.applyForce(wind);
    m.render();
    m.update();
    
    if (m.position.x > Window.right)
    {
      m.velocity.x *= -1;
      m.position.x = Window.right;
    }
    
    if (m.position.y < Window.bottom)
    {
      m.velocity.y *= -1; 
      m.position.y = Window.bottom;
    }
    
    if (m.position.x > Window.right - 150) 
    {
      m.applyFriction();  
      m.velocity.x *= 0.0;
    }  
  }
}
