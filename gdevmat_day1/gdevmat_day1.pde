void setup() //initialization
{
  size(1080, 720, P3D); //setup screen size (should be lower than native display)
  camera(0, 0, -(height/2) / tan(PI * 30 / 180), 
    0, 0, 0, 
    0, -1, 0); 
  waveWidth = width + 16;
  dX = (TWO_PI / period) * waveSpacing;
  yValues = new float[waveWidth / waveSpacing];
}

void draw() //called every frame
{
  background(128, 128, 128); //Background colour
  strokeWeight(1); //Thickness of line
  stroke(0, 0, 0); //Line colour
  
  //Cartesian Plane
  line(300, 0, -300, 0);
  line (0, 300, 0, -300);

  //Lines in Cartesian Plane
  for (int i = -300; i < 300; i += 10)
  {
    line (i, -5, i, 5);
    line(-5, i, 5, i);
  }

  drawLinearFunction();
  drawQuadraticFunction();
  drawCircle();
  createSineWave();
}

void drawLinearFunction()
{
  /*
     f(x) = x + 2;
   Let x = 4, then y = 6 (4,6)
   Let x = -5, then y = -3 (-5, -3)
   */

  stroke(0, 0, 128);
  for (int x = -200; x <= 200; x++)
  {
    circle(x, x + 2, 1);
  }
}

void drawQuadraticFunction()
{
  /*
    f(x) = 10x ^ 2 + 2x - 5;
   */

  stroke(0, 0, 225);
  for (float x = -300; x <= 300; x += 0.1f)
  {
    circle(10 * x, (float)Math.pow(x, 2) + (2 * x) - 5, 1);
  }
}

float radius = 50;

void drawCircle()
{
  stroke(160, 230, 255);
  for (int i = 0; i < 360; i++)
  {
    circle((float)Math.cos(i) * radius, (float)Math.sin(i) * radius, 1);
  }
}

//sine wave reference: https://processing.org/examples/sinewave.html
int waveSpacing = 1;
int waveWidth;
float theta = 0.0;
float waveHeight = 75.0;
float period = 250.0;
float dX;
float[] yValues;

void createSineWave()
{
  theta += 0.02;

  float xWave = theta;
  for (int i = 0; i < yValues.length; i++)
  {
    yValues[i] = sin(xWave) * waveHeight;
    xWave += dX;
  }

  stroke(255, 255, 255);
  for (int x = 0; x < yValues.length; x++)
  {
    circle((x + -550) * waveSpacing, yValues[x], 1);
  }
}
