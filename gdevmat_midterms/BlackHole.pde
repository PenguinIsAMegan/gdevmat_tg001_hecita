public class BlackHole
{
  public PVector position;
  private float scale = 50;
  
  BlackHole()
  {
    position = new PVector();
  }
  
  public void spawn()
  {
    position.x = random(Window.left, Window.right);
    position.y = random(Window.bottom, Window.top);
  }
  
  public void drawShape()
  {
    noStroke();
    fill(255);
    circle(position.x, position.y, scale);
  }
}
