public class OtherMatter
{
  public PVector position;
  private float[] colourProperties;
  private float xMean, standardDeviation, yMean, size, sizeMean, sizeStandardDeviation;
  private float gaussian = randomGaussian();
  
  OtherMatter()
  {
    position = new PVector();
    colourProperties = new float[3];
  }
  
  public void spawn()
  {
    standardDeviation = 50;
    xMean = random(Window.left, Window.right);
    yMean = random(Window.bottom, Window.top);
    sizeMean = random(25, 45);
    sizeStandardDeviation = 1;
    position.x = (standardDeviation * gaussian) + xMean;
    position.y = (standardDeviation * gaussian) + yMean;
    size = (sizeStandardDeviation * gaussian) + sizeMean;
   
    for (int i = 0; i < 3; i++)
    {
      float rand = random(255);
      colourProperties[i] = rand;
    }
  }
  
  public void drawShape()
  {
    noStroke();
    fill(colourProperties[0], colourProperties[1], colourProperties[2], 100);
    circle(position.x, position.y, size);
  }
}
