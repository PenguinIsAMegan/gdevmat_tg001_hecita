void setup()
{
  size(1080, 720, P3D);
  camera(0, 0, -(height/2) / tan(PI * 30 / 180), 
    0, 0, 0, 
    0, -1, 0);
    background(0);
   otherMatters = new OtherMatter[quantity];
   createScene();
}

void createScene()
{
  for (int i = 0; i < quantity; i++)
  {
    otherMatters[i] = new OtherMatter();
    otherMatters[i].spawn();
  }
  
  blackHole.spawn();
  blackHole.drawShape();
}

int quantity = 100;
BlackHole blackHole = new BlackHole();
OtherMatter[] otherMatters;
int num = 0;

void draw()
{
  background(0);
  
  num++;
  
  if (num >= quantity)
  {
    num = 0;
    createScene();
  }
  
  moveToBlackHole();
  
  blackHole.drawShape();
}

void spawnOtherMatter()
{
  for (int i = 0; i < quantity; i++)
  {
    otherMatters[i] = new OtherMatter();
    otherMatters[i].spawn();
  }
}

void moveToBlackHole()
{
  for (int i = 0; i < quantity; i++)
  {
    otherMatters[i].drawShape();
    PVector direction = PVector.sub(blackHole.position, otherMatters[i].position);
    direction.normalize();
    direction.mult(15);
    otherMatters[i].position.add(direction);
  }
}
