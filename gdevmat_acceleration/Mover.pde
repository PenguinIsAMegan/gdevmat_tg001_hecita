public class Mover
{
   PVector position = new PVector();
   PVector velocity = new PVector(); // rate of change in position
   PVector acceleration = new PVector(); // rate of change in velocity
   public float scale = 50;
   public float r = 255, g = 255, b = 255, a = 255;
   
   Mover()
   {
      position = new PVector(); 
   }
   
   Mover(float x, float y)
   {
      position = new PVector(x, y);
   }
   
   
   Mover(float x, float y, float scale)
   {
      position = new PVector(x, y);
      this.scale = scale;
   }
   
   Mover(PVector position)
   {
      this.position = position; 
   }
   
   Mover(PVector position, float scale)
   {
      this.position = position; 
      this.scale = scale;
   }
   
   
   public void render()
   {
      fill(r,g,b,a);
      update();
      circle(position.x, position.y, scale);
      
   }
   
   private void update()
   {
      // add the velocity each frame
      this.velocity.add(this.acceleration);
      // place limit
      this.velocity.limit(30);
      this.position.add(velocity);
   }
   
   public void setColor(float r, float g, float b, float a)
   {
      this.r = r;
      this.g = g;
      this.b = b;
      this.a = a;
   }
}
