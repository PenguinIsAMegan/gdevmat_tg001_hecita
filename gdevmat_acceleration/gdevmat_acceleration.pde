Mover mover;

void setup()
{
  size(1080, 720, P3D);
  camera(0, 0, -(height/2) / tan(PI * 30 / 180), 
    0, 0, 0, 
    0, -1, 0); 
   mover = new Mover();
   mover.position.x = Window.left + 50;
   mover.acceleration = new PVector(0.1, 0);
}

void draw()
{
  background(0);
  
  mover.render();
  
  if (mover.position.x > Window.right)
  {
    mover.position.x = Window.left + 50;
  }
  
 if (mover.position.x > (Window.left + 50) / 2 && mover.velocity.x > 0) 
 {
    mover.acceleration.x = -0.1;
 }
  
 if (mover.velocity.x <= 0)
 {
   mover.acceleration.x = 0;
   mover.velocity.x = 0;
 }
 
}
