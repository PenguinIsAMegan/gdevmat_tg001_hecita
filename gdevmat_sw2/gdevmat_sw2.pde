void setup()
{
  size(1080, 720, P3D);
  camera(0, 0, -(height/2) / tan(PI * 30 / 180), 
    0, 0, 0, 
    0, -1, 0); 
   background(0);
}

PVector mousePos()
{
  float x = mouseX - Window.windowWidth;
  float y = -(mouseY - Window.windowHeight);
  
  return new PVector(x, y);
}

float r = 1;
float g = 2;
float b = 3;

void draw()
{
  background(0);
  
  renderLine();
  r += 0.01f;
  g += 0.01f;
  b += 0.01f;
}

void renderLine()
{
  PVector mouse = mousePos();
  PVector light = mouse;
  PVector hilt = mouse; 
  float mappedValueR = map(noise(r), 0, 1, 0, 255); 
  float mappedValueG = map(noise(g), 0, 1, 0, 255);
  float mappedValueB = map(noise(b), 0, 1, 0, 255);
  
  light.normalize();
  light.mult(500);
  strokeWeight(15);
  stroke(mappedValueR, mappedValueG, mappedValueB, 255);
  line(0, 0, mouse.x, mouse.y);
  
  light.normalize();
  light.mult(500);
  strokeWeight(15);
  stroke(mappedValueR, mappedValueG, mappedValueB, 255);
  line(-mouse.x, -mouse.y, mouse.x, mouse.y);
  
  hilt.normalize();
  hilt.mult(100);
  strokeWeight(15);
  stroke(70,70,70);
  line(mouse.x, mouse.y, -mouse.x, -mouse.y);
}
