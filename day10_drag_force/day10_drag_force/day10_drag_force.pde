int qty = 10;
Mover[] movers = new Mover[qty];

Liquid ocean = new Liquid(0, -100, Window.right, Window.bottom, 0.1f);

void setup()
{
  size(1080, 720, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  
  float posX = Window.left + 100;
  for (int i = 0; i < qty; i++)
  {
    movers[i] = new Mover(posX, Window.top - 50);
    movers[i].mass = qty - i;
    movers[i].setColor(random(1, 255),random(1, 255),random(1, 255), random(150, 255));
    posX += 100;
  }
}

PVector wind = new PVector(0.1f, 0);
void draw()
{
  background(255);
  
  ocean.render();
  noStroke();
    
  for (int i = 0; i < qty; i++)
  {
    movers[i].applyGravity();
    movers[i].applyFriction();
    movers[i].render();
    movers[i].update();
      
    if (movers[i].position.x > Window.right)
    {
      movers[i].velocity.x *= -1;
      movers[i].position.x = Window.right;
    }
      
    if (movers[i].position.y < Window.bottom)
    {
      movers[i].velocity.y *= -1;
      movers[i].position.y = Window.bottom;
    }
      
    if (ocean.isCollidingWith(movers[i]))
    {
      movers[i].applyForce(ocean.calculateDragForce(movers[i]));
    }
    
    else
    {
      movers[i].applyForce(wind);
    }
  }
}
