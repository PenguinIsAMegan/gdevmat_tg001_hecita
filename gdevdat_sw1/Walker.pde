class Walker
{
  float xPosition;
  float yPosition;
  
  Walker()
  {
    
  }
  
  Walker(float x, float y)
  {
    xPosition = x;
    yPosition = y;
  }
  
  void render()
  {
    circle(xPosition, yPosition, 30);
  }
  
  void randomWalk()
  {
    int direction = floor(random(8));
    
    if (direction == 0)
    {
      yPosition += 10;
    }
    
    else if (direction == 1)
    {
      yPosition -= 10;
    }
    
    else if (direction == 2)
    {
      xPosition += 10;
    }
    
    else if (direction == 3)
    {
      xPosition -= 10;
    }
    
    else if (direction == 4)
    {
      yPosition += 10;
      xPosition += 10;
    }
    
    else if (direction == 5)
    {
      yPosition += 10;
      xPosition -= 10;
    }
    
    else if (direction == 6)
    {
      yPosition -= 10;
      xPosition += 10;
    }
    
    else if (direction == 7)
    {
      yPosition -= 10;
      xPosition -= 10;
    }
  }
}
