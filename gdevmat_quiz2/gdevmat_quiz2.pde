void setup()
{
  size(1080, 720, P3D);
  camera(0, 0, -(height/2) / tan(PI * 30 / 180), 
    0, 0, 0, 
    0, -1, 0); 
   background(0);
}

float t = 0;
float r = 1;
float g = 2;
float b = 3;
float x = Window.left;
float y = Window.bottom;
  
void draw()
{
  renderCircle();
  t += 0.01f;
  r += 0.01f;
  g += 0.01f;
  b += 0.01f;
  x += 0.01f;
  y += 0.01f;
}

void renderCircle()
{
  float size = noise(t);
  float mappedValueX = map(noise(x), 0, 1, Window.left, Window.right);
  float mappedValueY = map(noise(y), 0, 1, Window.bottom, Window.top);
  float mappedValueSize = map(noise(size), 0, 1, 0, 100);
  float mappedValueR = map(noise(r), 0, 1, 0, 255); 
  float mappedValueG = map(noise(g), 0, 1, 0, 255);
  float mappedValueB = map(noise(b), 0, 1, 0, 255);
  
  noStroke();
  fill(mappedValueR, mappedValueG, mappedValueB, 255);
  
  circle(mappedValueX, mappedValueY, mappedValueSize);
}
