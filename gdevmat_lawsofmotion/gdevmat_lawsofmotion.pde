Mover[] mover;
int quantity = 10;

void setup()
{
  size(1080, 720, P3D);
  camera(0, 0, Window.eyeZ, 
    0, 0, 0, 
    0, -1, 0); 
   mover = new Mover[quantity];
   
   for(int i = 0; i < quantity; i++)
   {
     mover[i] = new Mover();
     mover[i].mass = i + 1;
     mover[i].position.x = Window.left + 50;
     mover[i].acceleration = new PVector(0.1, 0);
   }
}

PVector wind = new PVector(0.1f, 0);
PVector gravity = new PVector(0, -1);

void draw()
{
  background(0);
  
  spawnObjects();
}

void spawnObjects()
{
  for(int i = 0; i < quantity; i++)
  {
    mover[i].update();
    mover[i].render();
    
    mover[i].applyForce(wind);
    mover[i].applyForce(gravity);
  }
}
