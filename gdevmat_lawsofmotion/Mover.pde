public class Mover
{
   PVector position = new PVector();
   PVector velocity = new PVector(); // rate of change in position
   PVector acceleration = new PVector(); // rate of change in velocity
   private float[] colourProperties;
   public float mass;
   
   Mover()
   {
      colourProperties = new float[3];
      setColour();
   }
 
   public void render()
   {
      noStroke();
      fill(colourProperties[0], colourProperties[1], colourProperties[2], 100);
      circle(position.x, position.y, this.mass * 10);
   }
   
   public void update()
   {
      // add the velocity each frame
      this.velocity.add(this.acceleration);
      // place limit
      this.velocity.limit(30);
      this.position.add(velocity);
      
      this.acceleration.mult(0); // reset acceleration to 0 to avoid over-value
      
      if (this.position.y < Window.bottom)
      {
        this.velocity.y *= -1; // 3rd law of motion
        this.position.y = Window.bottom; // safe guard
      }
  
      if (this.position.x > Window.right)
      {
        this.velocity.x *= -1; // 3rd law of motion
        this.position.x = Window.right; // safe guard
      }
      
      if (this.position.x < Window.left)
      {
        this.velocity.x *= 1; // 3rd law of motion
        this.position.x = Window.left; // safe guard
      }
   }
   
   public void setColour()
   {
     for (int i = 0; i < 3; i++)
     {
       float rand = random(255);
       colourProperties[i] = rand;
     }
   }
   
   public void applyForce(PVector force)
   {
     // A = F/M - second law of motion
     PVector f = PVector.div(force, this.mass);
     this.acceleration.add(f); 
   }
}
