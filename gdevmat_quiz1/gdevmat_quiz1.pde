void setup()
{
  size(1080, 720, P3D);
  camera(0, 0, -(height/2) / tan(PI * 30 / 180), 
    0, 0, 0, 
    0, -1, 0); 
   background(0);
}

int limit = 1000;

void draw()
{
  renderCircle();
  
  int time = frameCount;
  
  if (time == limit)
  {
    limit += 1000;
    setup();
  }
}

void renderCircle()
{
  float gaussian = randomGaussian();
  float mean = floor(random(-720, 720));
  float standardDeviation = 240;
  
  float x = (standardDeviation * gaussian) + mean;
  int y = floor(random(-720, 720));
  
  noStroke();
  fill(random(255), random(255), random(255), random(255));
  
  mean = floor(random(50));
  standardDeviation = 50;
  
  float randSize = (standardDeviation * gaussian) + mean;
  
  circle(x, y, randSize);
}
